//
//  CustomButton.swift
//  RaoViet
//
//  Created by Chung on 11/28/16.
//  Copyright © 2016 3i. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    required init?(coder aDecode: NSCoder){
        super.init(coder: aDecode)
        drawBoder()
    }
    func drawBoder() {
        layer.cornerRadius = 10.0
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(hex: "#eeeeee").cgColor
    }
}
