//
//  LoginViewController.swift
//  RaoViet
//
//  Created by Chung on 11/28/16.
//  Copyright © 2016 3i. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var tfUsername: CustomTextField!
    
    @IBOutlet weak var tfPassword: CustomTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        tfUsername.addIconForTextView("user")
        tfPassword.addIconForTextView("pass")
        
    }
    
}
